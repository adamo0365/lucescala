                                // carichiamo librerie
#include <ESP8266WiFi.h>        /*includiamo,ovvero carichiamo tutte le librerie che necessitano*/
#include <WiFiClient.h>         /* per gestione WiFi Web Server e EPROOM http client*/
#include <ESP8266WebServer.h>
                                //configuriamo WiFi
#ifndef STASSID                 /*IMPOSTIAMO SID E PASSWORD, il ssid è obbligatorio*/
#define STASSID "Ferraris"
#define STAPSK  "Pancaldo99"    /*omettendo la password la A.P. è aperto*/
#endif

const char *ssid = STASSID;     /* utiliziamo le credenziali inpostate in APSSID APPSK*/
const char *password = STAPSK;  /* settiamo e inizializiamo le variabili*/

int treshold = 512;             /*definisco il valore di soglia della luminosità che  siene restituito dal son off luce scala tramite http
                                  e assegno un valore default di 512*/
int pinRele = 12;               /* 5 è il pin usato con arduino 12 con son off*/
 

IPAddress staticIP(192,168,4,2); /*definisce ip statico con sottorete4*/
IPAddress gateway(192,168,4,1);  /*definisce acces point8luce scala)*/
IPAddress subnet(255,255,255,0); /*definisce indirizzo classe C ???*/

ESP8266WebServer server(80);      /* istanzia ogetto server di classe ESP8266WebServer con parametro del costruttore 80 che indica la porta*/

void setup(void) 
{
  WiFi.mode(WIFI_STA);            /* wifi standard*/
  WiFi.begin(ssid, password);     /* ci connettiamo con i dati indicati sopra*/
  WiFi.config(staticIP, gateway, subnet);
  pinMode(pinRele, OUTPUT);               /* pin controllo rele definito come outpup*/

                                          // aspettiamo la connessione
  while (WiFi.status() != WL_CONNECTED)   /* fino quando il Wi Fi non è connesso*/
    {
      delay(500);                         /* aspetto 500 millis*/
    }
 
                                          /* in risposta alla richiesta alla radice del server, eseguiamo una funzione anonima*/
  server.on("/fotoresistenza", []() 
  {
      String variabileIntermedia = server.arg(0); 
      treshold = variabileIntermedia.toInt();
                                         /* rispondiamo alla richiesta con codice HTTP 200, tipo MIME della risposta = testo semplice, ????
                                            risposta = valore di input analogico convertito in stringa:  ??????????*/
      server.send(200, "text/plain", String(analogRead(A0)) ); /* trametiamo al get il valore di analog read ( fotoresistenza)*/
      if( analogRead(A0) < treshold )                          /*se il valore della fotoresistenza è < del valore di soglia*/
        {                            
           digitalWrite(pinRele, LOW);                         /* spengo luce*/
        } 
      else 
        {
          digitalWrite(pinRele, HIGH);                         /* alrimenti acendo luce androne*/
        }
  }
  );
  
  server.begin();                                              /* facciamo avviare il server*/
}

void loop(void) 
{
  server.handleClient();               /* ogni iterazione del ciclo principale controlliamo se ci fosse una 
                                          richista HTTP in entrata ed eventualmente gli rispondiamo*/
}
