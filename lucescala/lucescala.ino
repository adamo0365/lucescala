#include <ESP8266WiFi.h>       /*includiamo,ovvero carichiamo tutte le librerie che necessitano*/
#include <WiFiClient.h>        /* per gestione WiFi Web Server e EPROOM http client*/
#include <ESP8266WebServer.h>
#include <ESP8266HTTPClient.h>
#include <EEPROM.h>

#ifndef APSSID                 /*IMPOSTIAMO SID E PASSWORD, il ssid è obbligatorio*/
#define APSSID "Ferraris"
#define APPSK  "Pancaldo99"    /*omettendo la password la A.P. è aperto*/
#endif

                               /* Se la riga sotto non è commentata funziona in seriale altrimenti no*/
#define SERIAL_DEBUG

WiFiClient client;             /* Istanziamo la classe WiFiClient dalla omonima libreria */
HTTPClient http;               /* Istanziamo la classe HTTPClient dalla libreria ESP8266HTTPClient */

const char *ssid = APSSID;     /* utiliziamo le credenziali inpostate in APSSID APPSK*/
const char *password = APPSK;
                               /*settiamo e inizializiamo le variabili*/
int statoPulsante;
/*int pinPulsante = 2;  //2 e 5 sono i piedi usati con arduino 0 e 12 con son off*/
/*int pinRele = 5;*/
int pinPulsante = 0; /* pin pulsante*/
int pinRele = 12;    /* pin relè*/
int trigger;         /* flag  di avvenuta e valida pressione pulsante luci*/
int statoLuci;       /* questa variabile ricalca lo stato delle luci assegnata a 1 se  la luce è accesa a 0 se spenta*/
int i;               /* usata nella funzione stat_puls come contatore*/
int x;               /* usata nel ciclo for come contatore*/
int mem;
int parto;           /* variabile posta a 1 quando il ciclo  delle luci scala è iniziato funziona come ritenuta*/
int fotocellula;     /* viene assegnato il valore reso dalla fotocellula crepuscolare del sonoff esterno (via http)*/
int eprMem;          /* il valore del tempo di accensione da conservare su eeprom  da recuperare in caso di black-out*/     
int potenziometro;   /* viene assegnato il valore del potenziometro che determina la soglia di luminosità*/
bool swCrepuscolare =true; /* usiamo questo booleamo per inibire l'accensione della luce quando
                              la fotoccellula posta sul sonoff esterno rileva luminosità diurna*/
unsigned long startWiFi;   /* valore di millis al momento della richiesta di partenza wifi*/
unsigned long timeNow;     /* viene assegnato il lalore istantaneo reso da millis()*/
unsigned long timer;       /* viene assegnato il valore della durata totale di accensione del luce scala  
                              questo valore viene assegnato in setup a seguito  della pressione di un pulsante 
                              entro X secondi da reset o prima accensione, altrimenti viene recuperato da eeprom*/

#ifdef SERIAL_DEBUG
  int wifiRequestDelay = 10000;  // se siamo in debug facciamo una richiesta ogni 10 secondi
#else
  int wifiRequestDelay = 120000; // altrimenti una ogni 2 minuti essendo piu stabile per il chip ESP8266
#endif

unsigned long timer1;   /* timer usato per definire il tempo di accensione in fase setup*/
unsigned long t0;       /* assegnato con il valore di millis  alla pressione valida del pulsante*/
unsigned long t1;       /* (durata accensione luci(timer) -12.500s) indica tempo inizio primo lampeggio*/ 
unsigned long t2;       /* (durata accensione luci(timer) -12.450s) indica tempo fine   primo lampeggio*/
unsigned long t3;       /* (durata accensione luci(timer) - 7.000s) indica tempo inizio secondo lampeggio*/  
unsigned long t4;       /* (durata accensione luci(timer) - 6.950s) indica tempo fine   secondo lampeggio*/
unsigned long t5;       /* (durata accensione luci(timer) - 5.500s) indica tempo inizio terzo lampeggio*/ 
unsigned long t6;       /* (durata accensione luci(timer) - 5.450s) indica tempo fine   terzo lampeggio*/

                                   /* dichiaro  le funzioni che definisco dopo void loop()*/
void stat_puls();                  /* stat_puls rileva e verifica validità pressione pulsante (antirimbalzo)*/
void controlloRele(bool eccitato); /* controlloRele controlla accensione e spegnimento del relè e quindi delle luci*/

void setup()
{
                                      /* Rilevamento tempo per settare il timer luci scala*/
  
  pinMode(pinRele,     OUTPUT);       /* definisco il pin del relè come output*/
  pinMode(pinPulsante, INPUT_PULLUP); /* definisco il pin pulsante come input in modalita pullup*/

  #ifdef SERIAL_DEBUG
    Serial.begin(9600);
  #endif
  
  Serial.println("Accensione");

  Serial.println("Inizializzo la EEPROM");
  EEPROM.begin(512);  /* inzializza la scrittura della eeprom */ 

  Serial.println("Attesa programmazione");
                     //un lampeggio delle luci segnala inizio  tempo utile per il setup tempo di accensione
  digitalWrite (pinRele, HIGH);
  delay(400);
  digitalWrite (pinRele, LOW);

  while (millis() <= 8000)                /* per 8 secondi il ciclo while attende la pressione di un pulsante*/
    { 
      ESP.wdtFeed();                      /* evita reset per intervento watchdog dopo 4 secondi*/
      if (digitalRead (pinPulsante) == 0) /* se rilevo pressione pulsante assegno a timer1 il valore di millis()*/
        {                                 /* che è il valore di millis nel momento in cui inizia il rilevamento*/
          Serial.println("Entro in programmazione");
          timer1 = millis();              /* della programmazione*/
    
          for (int x = 0; x < 4; x++)     /* 4 lampeggi rapidi segnalano inizio della fase di acquisizione tempo*/
            {
              digitalWrite (pinRele, HIGH);
              delay(250);
              digitalWrite (pinRele, LOW);
              delay(250);
             }
             
          //accendo la luce e aspetto la seconda pressione del pulsante che determina la durata del tempo di accensione
          digitalWrite (pinRele, HIGH);
          
          while(millis() < 255000) /* il ciclo while attende per 4 minuti e 15 sec la pressione di un pulsante */
              {
                ESP.wdtFeed();                  /* evita reset per intervento wacthdog*/
                if (digitalRead (pinPulsante) == 0) /* rilevo lettura pulsante*/
                  {
                    timer = millis() - timer1;  /* assegno a timer la durata di accensione luci scala*/ 
                                                /* (timer = valore di millis() rilevato al momento della seconda pressione del pulsante*/
                                                /* meno valore di millis alla prima pressione del pulsante */  
                    
                    if (timer < 25000)          /* se il tempo di accensione è minore di 25s imposto timer a 25 secondi*/
                    {                  
                        timer = 25000;
                    }                           /* in questo modo il tempo minimo impostabile è 25 sec*/
                    if (timer > 255000)         /* analogamente imposto il tempo massimo di accensione */
                    {                   
                        timer = 255000;
                    }                           /* in questo modo le luci scala non potranno restare 
                                                   accese per più di 4min 15sec*/
                    Serial.print("Tempo impostato a ");
                    Serial.println(timer);
                                                /* lampeggio per segnalare avvenuto set tempo luce*/
                    digitalWrite (pinRele, LOW); 
                    delay(200);
                    digitalWrite (pinRele, HIGH);
                    delay(200);
                    digitalWrite (pinRele, LOW);
                    
                    Serial.println("Scrivo il valore in EEPROM");
                    eprMem=timer/1000;         /* divido timer per mille per inserire in memoria un valore gestibile da un byte */
                    EEPROM.write(0,eprMem);    /* scrivo nella posizione 0 il valore  di eprMem */
                    EEPROM.commit();           /* scrive e chiude */ 
                    break;                     /* interrompiamo il loop */
                  }
              }
          }
  }

  Serial.print("Leggo il timer dalla EEPROM - Grezzo:");
  timer = (EEPROM.read(0))*1000;    /* ripristno a seguito blackout/riaccensione il valore del timer letto da eeprom*/
  Serial.print(timer);
  if (timer < 25000)                /* se < di 25000 è perche non si è mai settata la eeprom*/
  {
    timer = 60000;                  /* allora setto timer ad un valore predefinito di 1m */
  }
  Serial.print("; Finale:");
  Serial.print(timer/1000);
  Serial.println(" sec");

  Serial.println("Creo access point");
  WiFi.disconnect(true);        /* Più che disconnettere, cancella dalla eeprom i parametri di connessione usati finora;*/
  WiFi.persistent(false);       /* Evita che tali parametri vengano poi memorizzati*/
  WiFi.mode(WIFI_AP);           /* Imposta la modalità di connessione su Access Point*/
                                /* inizializzazione ESP 8266 come Access Point*/ 
  WiFi.softAP(ssid, password);  /* attiva accespoint con le pass e il sid che abbiamo definito*/
  startWiFi = millis();         /* rileva istante iniziale di avvio del wifi per poter inviare dopo 10 sec una seconda richiesta get*/
  
  Serial.println("Esco dal setup");
}

void loop() 
  {
    potenziometro = analogRead(0);                 /* leggo il valore del potenziometro per definire luminosità soglia luminosità*/
    
    if (millis() > (startWiFi + wifiRequestDelay)) /* ogni 2 minuti si richiede il valore della fotocellula e passiamo il valore del potenziometro*/
      {
        http.begin(client, String("http://192.168.4.2/fotoresistenza?treshold=") += potenziometro );/* Imposta i parametri della richiesta
                                                                                         definendo l'istanza della classe che verrà
                                                                                         utilizzata per l'instradamento della richiesta
                                                                                         e l'indirizzo url dentro il quale passa anche il valore 
                                                                                         del potenziometro */
        http.GET();                              /* Fa scattare la richiesta con il metodo GET */
        fotocellula = http.getString().toInt();  /* tramite il sonoff luce scala, su cui abbiamo installato un server WiFi */ 
                                                 /* richiedo al sonoff con la fotocellula, che ospita server web il valore */
                                                 /* reso dalla fotoresistenza */
        startWiFi = millis();                    /* azzero il timer del wifi per poter fare una successiva richiesta dopo 10/120 sec */
        
        if ( fotocellula > potenziometro) 
          {
           swCrepuscolare = true;
          }                                      /* se il valore fotocellula > del crepuscolare metto lo sw=1  perchè */ 
                                                 /* siamo in condizione in cui il relè luci scale può funzionare */
        else                                     /* altrimenti lo metto false per inibirlo */
          {
            swCrepuscolare=false;
          }
        Serial.print("Soglia: ");
        Serial.println(potenziometro);
        Serial.print("Fotocellula: ");
        Serial.println(fotocellula);
      }
    
    //  Diagramma temporale
    //                        on    off   on   off off  on
    //                                           on
    //             ________________    _______   _   _____
    //        ____|                |__|       |_| |_|     |______
    //                    A         B    C     D E F   G     H
    //            |                |  |       | | | |     |
    // t0 ________|                |  |       | | | |     |
    t1 = timer - 12500; //_________|  |       | | | |     |
    t2 = timer - 12450; //____________|       | | | |     |
    t3 = timer - 7000;  //____________________| | | |     |
    t4 = timer - 6950;  //______________________| | |     |
    t5 = timer - 5500;  //________________________| |     |
    t6 = timer - 5450;  //__________________________|     |
    // timer______________________________________________|
  
    stat_puls();                       /* invio a funzione stat_puls che legge e verifica pressione valida pulsante*/
    
    if ( trigger == 1 || parto == 1 )  /* se la funzione ha reso un pressione pulsante valida o il ciclo-luci è attivo*/
      {               
        parto = 1;                     /* assegno 1 a "parto" per avenuta partenza ciclo-luci, se già a uno ci resta*/
        timeNow = millis();            /* assegno a timenNow il valore di millis ()*/
      }
  
    if ( parto == 1 )                   /* se il ciclo luci è attivo*/
      {                                 /* nelle zone B,D o F  segnalo che il tempo del ciclo-luci sta per terminare facendo i lampeggi*/      
        if ( ((timeNow - t0) > t1 && (timeNow - t0) < t2)        /* controllo se mi trovo negli*/
             || ((timeNow - t0) > t3 && (timeNow - t0) < t4)     /* intervalli dove la lampada si spegne */
             || ((timeNow - t0) > t5 && (timeNow - t0) < t6) )   /* per fare i lampeggi di segnalazione*/
          {
            Serial.println("Lampeggio di segnalazione");
            statoLuci = 0;                             /* spengo la lampada per la durata stabilita*/
            controlloRele(false);                      /* per il lampeggio*/
          }
        else if (mem == 1)                             /* se fuori dagli inervalli (B-D-F) e ciclo luci attivo */                            
          {
            statoLuci = 1;                             /* la riaccendo se era accesa (mem =1)*/
            controlloRele(true);                       /* diversamente la lascio spenta*/
          }
    
     /* Fine tempo impostato quindi in zona H : resetto tutto*/
        
        if ( (timeNow - t0) >= timer )                 /* se millis inizio ciclo - millis presione pulsante >=  durata accensione luci*/
          {
            Serial.println("Tempo scaduto, spengo");
            statoLuci = 0;                             /* azzero tutti switch. variabili, rimetto ciclo in attesa pressione pulsante*/
            controlloRele(false);
            t0 = millis();
            mem = 0;
            parto = 0;
            trigger = 0;
          }
                      
                      
        if ( ((statoLuci == 1) && ((timeNow - t0) <= t1)) && trigger == 1 ) /* Viene premuto il pulsante quando la luce è accesa nella zona A*/
          {                                                                 /* la lampada è accesa e la spengo per*/
            mem = 0;                                                        /* non sprecare energia visto che sono*/
            statoLuci = 0;                                                  /* già arrivato al piano e ho ripremuto*/
            controlloRele(false);                                           /* il pulsante*/
            t0 = millis();
            parto = 0;
            Serial.println("Pulsante premuto, spengo");
          } 
                        
        else if ( ((statoLuci == 0) && ((timeNow - t0) <= t1)) && trigger == 1 ) /* se Viene premuto il pulsante quando la luce è spenta*/
          {
            Serial.println("Accendo le luci");
            mem = 1;                            
            statoLuci = 1;                                /* metto a 1 le variabile che indicalo stato della luce*/
            controlloRele(true);                          /* accendo luce*/
          }
    
        
        if ( ((timeNow - t0) > t1) && trigger == 1 )      /* se Viene premuto il pulsante dopo il primo lampeggio e resetto il timer*/
          {
            Serial.println("Pulsante premuto, prolungo il tempo");
            mem = 1;                                      /* ribadisco le variabili nel loro stato e l'accensione  delle luci*/
            statoLuci = 1;
            controlloRele(true);
            t0 = millis();                                /* resetto il timer inserendo in t0 il nuovo valore di millis alla pressione del pulsante */
            parto = 1;
          }
      }
  }         
                    /* Funzione per rilevamento pressione pulsante*/
void stat_puls() 
  {
    statoPulsante = digitalRead(pinPulsante); /* leggo il pulsant*/
    
    trigger = 0;                         /* azzero la variabile che conferma il valida pressione pulsante*/
  
    if ( statoPulsante == 0 )            /* se pulsante premuto( è inverso per via del pull-up)*/
      {
        i = i + 1;                       /* incremento contatore*/
      }
    else if ( i > 10 && statoPulsante == 1 )  /* se eseguita 10 vole la funzione con pulsante continuativamate premuto*/
      {
        trigger = 1;                     /* metto a 1 trigger per avvenuta valida pressione pulsante*/
        i = 0;                           /* azzero contatore per prossimo ciclo*/
        if ( (timeNow - t0) < t1 )       /* se la pressione avviene in zona A */
          {
            t0 = millis();               /* faccio ripartire il tempo mettento t0 = millis()*/
          } 
       }
    else                                 /* se la funzione non è ancora stata ripetuta 10 volte ma il pulsante torna a 1*/
       {
          i = 0;                         /* azzero indice */
       } 
       //è necessario , nel caso la funzione non sia ancora ciclata 10 volte ma il pulsante viene rilasciato ( pressione  non valida perche corta//
       //rimettere a zero  l'indice altrimenti i prpssomo coclo la verifica  è più breve  anche ineficace§
       
      
    }

void controlloRele(bool eccitato)           /*controllando  il relè chiaramente accendo o spengo le scale*/
  {
    if ( eccitato && swCrepuscolare )       /* il parametro "eccitato" assume il valore che gli viene passato alla chiamata della funzione*/
       {                                    /* che può essere true o false*/
        digitalWrite (pinRele, HIGH);       /* se eccitato e true e crepuscolare pure accendo la luce scala*/
       } 
    else 
      {
        digitalWrite (pinRele, LOW);         /* se uno dei due non è true  spengo tutto*/
      }
  }
 //                               bozza di funzione che gestisce i lampeggi                                 
 //void blinking (lampeggi, intervallo)
 // for (l =0; l>=lampeggi; l++)
 //   { 
 //     state=false:
 //      
 //      delay(intervallo);
 //      state =!state;
 //      controllorelè(state);  
 //   }
 
